import {BehaviorSubject, Observable} from 'rxjs';

export interface AutotypeConfig {
  delay?: number;
}

export class Autotype {

  private readonly delay;
  private content = new BehaviorSubject('');

  public get text(): Observable<string> {
    return this.content.asObservable();
  }

  constructor(config: AutotypeConfig = {}) {
    if (config && config.delay && config.delay) {
      this.delay = config.delay;
    } else {
      this.delay = 0;
    }
  }

  set(text: string = ''): Promise<string> {
    this.content.next(text);

    return new Promise((resolve, _) => {
      this.content.next(text);

      setTimeout(() => {
        resolve(text);
      }, this.delay);
    });
  }

  async type(text: string = '') {
    for (let i = 0; i < text.length; ++i) {
      await this.append(text.charAt(i));
    }
  }

  append(key: string = ''): Promise<string> {
    return new Promise((resolve, _) => {
      const next = this.content.value + key;
      this.content.next(next);

      setTimeout(() => {
        resolve(next);
      }, this.delay);
    });
  }

  async delete(characters: number = 0) {
    for (let i = 0; i < characters; ++i) {
      await this.back();
    }
  }

  back(): Promise<string> {
    return new Promise((resolve, _) => {
      const length = this.content.value.length;
      const next = length > 0 ? this.content.value.substr(0, length - 1) : '';
      this.content.next(next);

      setTimeout(() => {
        resolve(next);
      }, this.delay);
    });
  }

  clear(): Promise<string> {
    return new Promise((resolve, _) => {
      const next = '';
      this.content.next(next);

      setTimeout(() => {
        resolve(next);
      }, this.delay);
    });
  }

  wait(ticks: number = 0): Promise<string> {
    return new Promise((resolve, _) => {
      const next = this.content.value;
      this.content.next(next);

      setTimeout(() => {
        resolve(next);
      }, ticks * this.delay);
    });
  }
}
