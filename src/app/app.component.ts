import {Component, OnInit} from '@angular/core';
import {Autotype} from "./autotype/autotype";
import {BehaviorSubject} from "rxjs";
import {animate, state, style, transition, trigger} from '@angular/animations';
import confetti from 'canvas-confetti';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger(
      'playAnimation',
      [
        state('visible',
          style({opacity: 1})
        ),
        state('hidden',
          style({opacity: 0})
        ),
        transition('visible => hidden', [
          animate('0.5s')
        ]),
        transition('hidden => visible', [
          animate('0.5s')
        ])
      ]
    )
  ]
})
export class AppComponent implements OnInit {

  autoLine1 = new Autotype({delay: 100});
  autoLine2 = new Autotype({delay: 100});

  running = new BehaviorSubject<Boolean>(false);
  pause = new BehaviorSubject<Boolean>(true);

  get blinkClass(): string {
    if (this.pause.value) {
      return 'blink';
    }

    return '';
  }

  get playable(): boolean {
    return !this.running.value && this.pause.value;
  }

  async ngOnInit(): Promise<void> {
    this.pause.next(false);
    await this.autoLine1.type("Klicke, um mehr zu erfahren!");
    this.pause.next(true);

    this.running.asObservable().subscribe(async running => {
      if (running) {
        await this.type("Werte Charlotte von Bettenhausen,");
        await this.autoLine1.wait(5);
        await this.type("5 mal haben wir bereits die Sonne umrundet,");
        await this.type("seitdem Sie mit uns gemeinsam");
        await this.type("in den Krieg gegen Skrale,");
        await this.type("Wardraks und Gors zogen,");
        await this.type("um Ihren 23sten Geburtstag gebührend zu feiern.");
        await this.autoLine1.wait(5);
        await this.type("Die Zeiten ändern sich");
        await this.type("und so auch");
        await this.type("die Übermittlung unserer Glückwünsche.");
        await this.autoLine1.wait(5);
        await this.type("Daher senden wir auf diesem Wege");
        await this.type("unsere Gratulationen.");
        await this.autoLine1.wait(5);
        await this.type("Wir wünschen (nicht nur für heute)");
        await this.type("viel Erfolg, Liebe, Gesundheit");
        await this.type("und einen aufregenden Tag");
        await this.type("im Kreise Ihrer Liebsten.");
        await this.autoLine1.wait(5);
        await this.type("Happy Birthday!");
        await this.type("(diesen neuartigen Sprech zwitscherte uns ein Vögelchen)", this.autoLine2);
      }
    });
  }

  run(): void {
    if (this.playable) {
      this.running.next(true);
    }

    this.fire();
  }

  private fire(): void {
    const randomInRange = (min: number, max: number) => {
      return Math.random() * (max - min) + min;
    }

    const duration = 120 * 1000;
    const animationEnd = Date.now() + duration;
    const defaults = {startVelocity: 30, spread: 360, ticks: 60, zIndex: 0};

    const interval = setInterval(function () {
      const timeLeft = animationEnd - Date.now();

      if (timeLeft <= 0) {
        return clearInterval(interval);
      }

      const particleCount = 50 * (timeLeft / duration);
      confetti(Object.assign({}, defaults, {
        particleCount,
        origin: {x: randomInRange(0.1, 0.3), y: Math.random() - 0.2}
      }));
      confetti(Object.assign({}, defaults, {
        particleCount,
        origin: {x: randomInRange(0.7, 0.9), y: Math.random() - 0.2}
      }));
    }, 250);
  }

  private async type(text: string, autotype: Autotype = this.autoLine1) {
    this.pause.next(false);
    await autotype.clear();
    await autotype.type(text);
    this.pause.next(true);
    await autotype.wait(5);
  }
}
